@tool
extends Node2D

## HOWTO:
## Make sure main scene is open. etc.
## Double-click "res://new_gradient_texture_2d.tres" to see it in the Inspector.
## Move the fill from and to widgets in the tool at the very top.
## Notice that there is no printout
## Now open the Fill subsection and change the settings there, see that the
## _inspector_prop_edited func now runs.
## Conclusion: the inspect.property_edited is somehow ignoring the top widget.

func _ready() -> void:
	var inspect = EditorInterface.get_inspector()
	if inspect.is_connected(&"property_edited",_inspector_prop_edited):
		inspect.property_edited.disconnect(_inspector_prop_edited)
	inspect.property_edited.connect(_inspector_prop_edited)
	print(inspect.get_signal_connection_list(&"property_edited"))

func _inspector_prop_edited(p:String):
	print("_inspector_prop_edited:", p)
